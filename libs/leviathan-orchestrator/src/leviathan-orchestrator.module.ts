import { DynamicModule, Module } from '@nestjs/common';
import { LeviathanOrchestratorService } from './services/leviathan-orchestrator.service';
import { LeviathanLoggerService } from './services/leviathan-logger.service';
import { LeviathanConfiguration } from './leviathan.models';
import { LeviathanHttpService } from './services/leviathan-http.service';

let configuration: LeviathanConfiguration;

@Module({
  providers: [LeviathanLoggerService, LeviathanHttpService],
})
export class LeviathanOrchestratorModule {
  static leviathanConfigure(options: LeviathanConfiguration): DynamicModule {
    configuration = options;
    return {
      module: LeviathanOrchestratorModule,
      providers: [
        {
          provide: 'CONFIG',
          useValue: options,
        },
        LeviathanOrchestratorService,
      ],
    };
  }
}
