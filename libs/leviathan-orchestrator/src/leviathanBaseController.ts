import { HttpServer } from '@nestjs/common';
import { LeviathanHttpService } from './services/leviathan-http.service';
import { Request, Response } from 'express';
import {
  LeviathanApiCall,
  LeviathanApiCallType,
  LeviathanController,
  LeviathanControllerMemory,
} from './leviathan.models';

export class LeviathanBaseController {
  constructor(
    private basePath: string,
    private controllerConf: LeviathanController,
    private httpServer: HttpServer,
    private httpService: LeviathanHttpService,
  ) {
    const path = `${this.basePath}${controllerConf.path}`;

    switch (controllerConf.type) {
      case LeviathanApiCallType.GET:
        this.httpServer.get(path, (req, res) => this.handle(req, res));
        break;
      case LeviathanApiCallType.POST:
        this.httpServer.post(path, (req, res) => this.handle(req, res));
        break;
      case LeviathanApiCallType.PATCH:
        this.httpServer.patch(path, (req, res) => this.handle(req, res));
        break;
      case LeviathanApiCallType.PUT:
        this.httpServer.put(path, (req, res) => this.handle(req, res));
        break;
      case LeviathanApiCallType.DELETE:
        this.httpServer.delete(path, (req, res) => this.handle(req, res));
        break;
    }
  }

  public handle(req: Request, res: Response): void {
    this.execute(req).then((value) => {
      res.send(value);
    });
  }

  private async execute(req: Request): Promise<any> {
    // controller mem space
    let mem: LeviathanControllerMemory = {
      data: [],
      reqBody: req.body,
      reqQuery: req.query,
      reqHeader: req.headers,
      ...this.controllerConf.mem,
    };

    if (this.controllerConf.callList?.length > 0) {
      await this.apiExecuter(this.controllerConf.callList, mem);
    }
    if (this.controllerConf.aggregator !== undefined) {
      this.controllerConf.aggregator.aggregate(mem);
    }

    // copy creation to free controller memory
    return JSON.parse(
      JSON.stringify(mem.res !== undefined ? mem.res : mem.data),
    );
  }

  private async apiExecuter(
    apis: LeviathanApiCall[],
    mem: LeviathanControllerMemory,
  ): Promise<void> {
    const promises: Promise<any>[] = [];

    for (const api of apis) {
      const url: string = typeof api.url === 'string' ? api.url : api.url(mem);

      const apiCall = this.httpService.request(url, {
        method: api.type,
      });

      if (api.async) {
        promises.push(apiCall);
      } else {
        const resp = await apiCall;
        mem.data.push(resp);
      }
    }

    if (promises.length > 0) {
      const resp = await Promise.all(promises);
      mem.data.push(...resp);
    }
  }
}