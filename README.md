<p align="center"> <a href="https://bitbucket.org/raffaelemancino/trendkill-sql-micro" target="blank"> <img src="https://bitbucket.org/raffaelemancino/leviathan-orchestrator/raw/353412ac2917604fc4ea77a225840e77978a850c/whale.png" width="150" alt="Trendkill Logo" /> </a> </p>

Leviathan
=========

### Description

Leviathan is a configurable Nestjs Module for orchestrator creation.
[comment]: <> (Use **mysql** for MySQL and MariaDB and **pg** for PostgreSQL)

### Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

[comment]: <> (### Test)

[comment]: <> (```bash)

[comment]: <> (# unit tests)

[comment]: <> ($ npm run test)

[comment]: <> (# e2e tests)

[comment]: <> ($ npm run test:e2e)

[comment]: <> (# test coverage)

[comment]: <> ($ npm run test:cov)

[comment]: <> (```)

### Support

Trendkill is an GPLv2-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them.

### Stay in touch

Author - [Raffaele F. Mancino](https://bitbucket.org/raffaelemancino/)

### License

Nest is [GNU GPLv2 licensed](LICENSE).
