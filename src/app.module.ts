import { Module } from '@nestjs/common';
import { LeviathanOrchestratorModule } from './leviathan/leviathan-orchestrator.module';
import {
  LeviathanApiCallType,
  LeviathanConfiguration,
} from './leviathan/leviathan.models';

const leviathanConfiguration: LeviathanConfiguration = {
  name: 'Leviathan Host',
  controllers: [
    {
      name: 'Test Get',
      path: '/test-get',
      type: LeviathanApiCallType.GET,
      aggregator: {
        aggregate(mem): void {
          mem.res = mem.reqQuery;
        },
      },
    },
    {
      name: 'Test Post',
      path: '/test-post',
      type: LeviathanApiCallType.POST,
      aggregator: {
        aggregate(data): void {
          data.res = data.reqBody;
        },
      },
    },
    {
      name: 'Test Annidate',
      path: '/raf/test',
      mem: {
        hello: 'world',
      },
      type: LeviathanApiCallType.GET,
      callList: [
        {
          url: 'https://gorest.co.in/public/v1/users',
          async: false,
          type: LeviathanApiCallType.GET,
        },
        {
          url: (data) => {
            return `https://gorest.co.in/public/v1/${data.reqQuery['destination']}`;
          },
          async: true,
          type: LeviathanApiCallType.GET,
        },
      ],
      aggregator: {
        aggregate(data): void {
          data.res = JSON.parse(JSON.stringify(data));
        },
      },
    },
  ],
};

@Module({
  imports: [
    LeviathanOrchestratorModule.leviathanConfigure(leviathanConfiguration),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
