import { HttpServer, Inject, Injectable } from '@nestjs/common';
import { LeviathanLoggerService } from './leviathan-logger.service';
import { LeviathanConfiguration } from '../leviathan.models';
import { HttpAdapterHost } from '@nestjs/core';
import { LeviathanHttpService } from './leviathan-http.service';
import { json } from 'express';
import { LeviathanBaseController } from '../leviathanBaseController';

@Injectable()
export class LeviathanOrchestratorService {
  constructor(
    private leviathanLoggerService: LeviathanLoggerService,
    @Inject('CONFIG') private options: LeviathanConfiguration,
    private leviathanHttpService: LeviathanHttpService,
    private host: HttpAdapterHost,
  ) {
    this.leviathanLoggerService.debug(
      `Leviathan started with name: ${options.name}!`,
    );

    this.options.basePath = this.options.basePath ?? '/leviathan';

    this.leviathanLoggerService.debug(
      `Leviathan listening on route: ${options.basePath}!`,
    );

    const controllers = this.options.controllers.map<string>((controller) => {
      return `${controller.type} ${controller.path}`;
    });

    this.leviathanLoggerService.debug(
      `Leviathan available controllers: ${controllers.join(', ')}`,
    );
    this.init();
  }

  private init(): void {
    const httpserver: HttpServer =
      this.host.httpAdapter.getInstance<HttpServer>();
    httpserver.use(json());

    this.options.controllers.forEach((controllerConf) => {
      const controller = new LeviathanBaseController(
        this.options.basePath,
        controllerConf,
        httpserver,
        this.leviathanHttpService,
      );
    });
  }
}
